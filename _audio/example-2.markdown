---
layout: example
title:  Example 2
subtitle: The sound module (part 2)
author: Erwin Goossen
script: /js/audio/example2.js
form: audio/example2.html
intro: The finished javascript code for a sound module
---
### The Code

{% highlight javascript %}
/*global app, app.ac */
/*global app, self.context */
var Sound;
Sound = function (file, context) {
    'use strict';
    var self = this;

    self.context = null;
    self.buffer = null;
    self.path = '';
    self.mute = false;
    self.gain = 0.7;
    self.pan = 0;
    self.filter = false;
    self.filterQ = 1;
    self.filterFreq = 8000;

    /**
     * Set one of the properties values
     *
     * @param {string} key   Name of th eproperty to set
     * @param {any} value The value
     */
    function set(key, value) {
        if (key[0] === '_') {
            return false;
        }
        self[key] = value;
    }

    /**
     * Get the value of the given key
     *
     * @param  {string} key The name of the value
     *
     * @return {any} 
     */
    function get(key) {
        if (key[0] === '_') {
            return false;
        }
        return self[key];
    }

    /**
     * Create the audioContextBuffer, set the gain and other values.
     * Connect all the parts together and play the sound.
     */
    function play() {
        /**
         * Create the buffersource and fill it with our buffer
         */
        var acSound = self.context.createBufferSource(),
            acGain = self.context.createGain(),
            acPanner = false,
            acFilter = self.context.createBiquadFilter();

        acSound.buffer = self.buffer;

        if (self.context.createStereoPanner !== undefined) {
            acPanner = self.context.createStereoPanner();
            acPanner.pan.value = self.pan;
        }
        /**
         * Set the Gain, Panner, and Filter
         */
        if (self.mute) {
            acGain.gain.value = 0;
        } else {
            acGain.gain.value = self.gain;
        }
        acFilter.Q.value = self.filterQ;
        acFilter.frequency.value = self.filterFreq;

        /**
         * Connect everything together
         */
        if (self.filter) {
            if (acPanner) {
                acSound.connect(acPanner);
                acPanner.connect(acFilter);                
            } else {
                acSound.connect(acFilter);
            }
            acFilter.connect(acGain);
        } else {
            if (acPanner) {
                acSound.connect(acPanner);
                acPanner.connect(acGain);                
            } else {
                acSound.connect(acGain);
            }
        }
        acGain.connect(self.context.destination);

        /**
         * Play the sound
         */
        acSound.start(0);
    }

    function getFile() {
        var httpRequest = new XMLHttpRequest();
        httpRequest.onload = function () {
            self.context.decodeAudioData(httpRequest.response, function (buffer) {
                self.buffer = null;
                self.buffer = buffer;
            });
        };
        httpRequest.open('GET', self.path, true);
        httpRequest.responseType = 'arraybuffer';
        httpRequest.send(null);
    }

    function _init(file, context) {
        self.path = file;
        self.context = context;
        getFile();
    }

    _init(file, context);

    return {
        play: play,
        setSound: getFile,
        set: set,
        get: get
    };
};
{% endhighlight %}

> sound downloaded from: [http://www.musicradar.com/news/tech/sampleradar-502-free-80s-samples-233696][musicradar]

[musicradar]: http://www.musicradar.com/news/tech/sampleradar-502-free-80s-samples-233696