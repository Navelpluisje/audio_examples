var example1 = (function () {
    var yourContext,
        yourBuffer,
        yourSound,
        yourGain;

    /**
     * We're getting the file and put it;s value into yourBuffer
     * This way we can use it multiple times
     */
    function _getFile() {
        var httpRequest = new XMLHttpRequest();
        httpRequest.onload = function () {
            yourContext.decodeAudioData(httpRequest.response, function (buffer) {
                yourBuffer = null;
                yourBuffer = buffer;
            });
        };
        httpRequest.open('GET', '/assets/example1.wav', true);
        httpRequest.responseType = 'arraybuffer';
        httpRequest.send(null);
    }

    /**
     * Set the context.
     * Check if we have the audio api en assign it to youtContext
     */
    function _setContext() {
        var context = null;
        try {
            context = window.AudioContext || window.webkitAudioContext || false;
            if (context) {
                yourContext = new context(); 
            }
            return true;
        } catch (e) {
            alert("This example doesn't seem to be available for your browser. Sorry about that. We recommend Firefox or Chrome")
        }
        return false;
    }

    /**
     * Now we can buid and connect everything together and start the sound
     */
    function _playSound() {
        /**
         * Create the sound by pasring yourBuffer to the Sound
         */
        yourSound = yourContext.createBufferSource();
        yourSound.buffer = yourBuffer;

        /**
         * Now we create our Gain and set it's value
         */
        yourGain = yourContext.createGain();
        yourGain.gain.value = .7;

        /**
         * Chain it all together
         */
        yourSound.connect(yourGain);
        yourGain.connect(yourContext.destination);

        /**
         * And finaly play the sound by calling start
         */
        yourSound.start(0);
    }

    /**
     * Yust a small init for prefetching the sample and creating the context
     */
    function _init() {
        _setContext();
        _getFile();
    }

    _init();

    /**
     * By returning play we can call this function from everywhere
     */
    return {
        play: _playSound
    }

}());